import React, { Component } from "react";
import $ from "jquery";
import { Route, HashRouter } from "react-router-dom";
import dropify from "dropify";
import tablesorter from "tablesorter";

import "../node_modules/dropify/src/sass/dropify.scss";

import "./asset/scss/index.scss";
import "./asset/js/jquery-master.js";

import Navbar from "./components/Navbar";
import SideBar from "./components/Sidebar";
import MainContent from "./components/MainContent";

import Folder from "./pages/Folder";
import Button from "./pages/Button";
import DataTable from "./pages/Data-Table";
import Form from "./pages/Form";
import Modal from "./pages/Modal";
import Tabs from "./pages/Tabs";
import Wizard from "./pages/Wizard";
import Table from "./pages/Table";
import Login from "./pages/Login";
// - - - - - - - - - - - - - - - -
import Member from "./pages/Member";
import Voucher from "./pages/Voucher";
import VoucherAdd from "./pages/Voucher-Add";

class App extends Component {
  render() {
    return (
      <HashRouter>
        <div className="master-body">
          <Navbar />
          <div className="header-frame" />
          <div className="master-frame">
            <SideBar />
            <Route exact path="/" component={MainContent}></Route>
            <Route path="/Folder" component={Folder}></Route>
            <Route path="/Button" component={Button}></Route>
            <Route path="/DataTable" component={DataTable}></Route>
            <Route path="/Form" component={Form}></Route>
            <Route path="/Modal" component={Modal}></Route>
            <Route path="/Tabs" component={Tabs}></Route>
            <Route path="/Wizard" component={Wizard}></Route>
            <Route path="/Table" component={Table}></Route>

            <Route path="/Member" component={Member}></Route>
            <Route path="/Voucher" component={Voucher}></Route>
            <Route path="/VoucherAdd" component={VoucherAdd}></Route>
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default App;
