import React, { Component } from "react";
import { Route, HashRouter } from "react-router-dom";
import ReactDOM from "react-dom";

import App from "./App";
import Login from "./Login";

class Index extends Component {
  render() {
    return (
      <HashRouter>
        <Route path="/App" component={App}></Route>
        <Route exact path="/Login" component={Login}></Route>
      </HashRouter>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
