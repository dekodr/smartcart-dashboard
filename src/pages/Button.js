import React, { Component } from "react";
import $ from "jquery";

class ButtonScreen extends Component {
  render() {
    return (
      <div className="right-panel">
        <div className="p-20-c">
          <div className="breadcrumb">
            <div className="page-title">Buttons</div>
            <div className="page-info">
              <div className="breadcrumb-item">UI Elements</div>
              <div className="breadcrumb-item">Buttons</div>
            </div>
          </div>
        </div>
        <div className="p-20-c">
          <div className="row" id="index-first-row">
            <div className="col-6">
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        <h5>Basic Buttons</h5>
                      </div>
                      <div className="separator"></div>

                      <div className="card-content">
                        <p><code>.btn</code> class can be use with color classes such <code>.btn-primary</code>, <code>.btn-success</code>, <code>.btn-warning</code>, <code>.btn-danger</code>, <code>.btn-info</code> and <code>.btn-link</code></p>
                        <div className="button-row">
                          <button className="btn btn-primary">primary</button>
                          <button className="btn btn-success">success</button>
                          <button className="btn btn-warning">warning</button>
                          <button className="btn btn-danger">danger</button>
                          <button className="btn btn-info">info</button>
                          <button className="btn btn-link">link</button>
                        </div>
                        <div className="separator separator-dashed"></div>
                        
                        <p>The <code>.btn</code> classes are designed to be used with <code>&lt;button&gt;</code>, <code>&lt;a&gt;</code> and <code>&lt;input&gt;</code> elements.</p>
                        <div className="button-row">
                          <a className="btn btn-primary">link</a>
                          <button className="btn btn-success">button</button>
                          <input type="button" value="Input" className="btn btn-info" />
                          <input type="submit" value="Submit" className="btn btn-warning" />
                          <input type="reset" value="Reset" className="btn btn-danger" />
                        </div>
                        <div className="separator separator-dashed"></div>

                        <p>states of <code>.btn</code> class used with <code>.active</code>, <code>.disabled</code> class or  <code>disabled</code> attribute</p>
                        <div className="button-row">
                          <button className="btn btn-primary active">active</button>
                          <button className="btn btn-success active">active</button>
                          <button className="btn btn-primary-outline active">outline</button>
                          <button className="btn btn-danger disabled" disabled>disabled</button>
                          <button className="btn btn-warning disabled" disabled>disabled</button>
                          <button className="btn btn-info-outline disabled" disabled>disabled</button>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>   
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        <h5>Button with Icon</h5>
                      </div>
                      <div className="separator"></div>

                      <div className="card-content">
                        <p>on <code>.btn</code> can be used with icon also</p>
                        <div className="button-row">
                          <button className="btn btn-primary"><i className="fas fa-dollar-sign"></i>Primary</button>
                          <button className="btn btn-info"><i className="fas fa-info-circle"></i>info</button>
                          <button className="btn btn-success-outline"><i className="far fa-edit"></i>edit</button>
                          <button className="btn btn-danger-hover-outline"><i className="fas fa-sign-out-alt"></i>Logout</button>
                        </div>
                        <div className="separator separator-dashed"></div>

                        <p><code>.btn-icon</code> used to have an icon only</p>
                        <div className="button-row">
                          <button className="btn btn-primary btn-icon "><i className="fas fa-dollar-sign"></i></button>
                          <button className="btn btn-info btn-icon "><i className="fas fa-info-circle"></i></button>
                          <button className="btn btn-success btn-icon "><i className="far fa-edit"></i></button>
                          <button className="btn btn-danger-hover-outline btn-icon "><i className="fas fa-sign-out-alt"></i></button>
                        </div>
                        <div className="separator separator-dashed"></div>

                        <p><code>.btn-icon</code> also two other size variations, including <code>.btn-icon-lg</code> and <code>.btn-icon-sm</code></p>
                        <div className="button-row">
                          <button className="btn btn-primary btn-icon btn-icon-lg btn-rad btn-elevate"><i className="fas fa-dollar-sign"></i></button>
                          <button className="btn btn-info btn-icon "><i className="fas fa-info-circle"></i></button>
                          <button className="btn btn-success btn-icon btn-icon-sm"><i className="far fa-edit"></i></button>
                          <button className="btn btn-danger-hover-outline btn-icon btn-icon-sm"><i className="fas fa-sign-out-alt"></i></button>
                        </div>
                        <div className="separator separator-dashed"></div>

                        <p><code>.btn-icon</code> also can be used with <code>.btn-rad</code> and others, like <code>.btn-elevate</code></p>
                        <div className="button-row">
                          <button className="btn btn-primary btn-icon btn-rad btn-elevate"><i className="fas fa-dollar-sign"></i></button>
                          <button className="btn btn-info btn-icon btn-rad btn-elevate"><i className="fas fa-info-circle"></i></button>
                          <button className="btn btn-success btn-icon btn-rad btn-elevate"><i className="far fa-edit"></i></button>
                          <button className="btn btn-danger-hover-outline btn-icon btn-rad btn-elevate"><i className="fas fa-sign-out-alt"></i></button>
                        </div>
                        <div className="separator separator-dashed"></div>

                        <p><code>.btn-animated</code> used to have some kinda animation between icon and text</p>
                        <div className="button-row">
                          <button className="btn btn-primary btn-cons btn-animated"><i className="fas fa-envelope"></i><span>animate</span></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        <h5>Button as Label</h5>
                      </div>
                      <div className="separator"></div>

                      <div className="card-content">
                        <p>p can be uses with <code>.label</code>, class color and class size</p>
                        <p>
                          <code>
                            &lt;p className="label label-danger label-lg"&gt;label&lt;/p&gt; <br/>
                            &lt;p className="label label-warning"&gt;label&lt;/p&gt; <br/>
                            &lt;p className="label label-primary label-sm"&gt;label&lt;/p&gt; <br/>
                            &lt;p className="label label-info label-xs"&gt;label&lt;/p&gt;
                          </code>
                        </p>
                        <div className="button-row">
                          <p className="label label-danger label-lg">label</p>
                          <p className="label label-warning">label</p>
                          <p className="label label-primary label-sm">label</p>
                          <p className="label label-info label-xs">label</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-6">
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        <h5>Button Customization</h5>
                      </div>
                      <div className="separator"></div>

                      <div className="card-content">
                        <p><code>.btn</code> class also have variety of sizing used with <code>.btn-lg</code>, <code>.btn-sm</code> and <code>.btn-xs</code></p>
                        <div className="button-row">
                          <button className="btn btn-primary btn-lg">Large button</button>
                          <button className="btn btn-info">default button</button>
                          <button className="btn btn-warning btn-sm">small button</button>
                          <button className="btn btn-danger btn-xs">tiny</button>
                        </div>
                        <div className="separator separator-dashed"></div>

                        <p><code>.btn</code> class also can be constanly at the same width using <code>.btn-cons</code></p>
                        <div className="button-row">
                          <button className="btn btn-success btn-cons">same width</button>
                          <button className="btn btn-warning btn-cons">same width</button>
                          <button className="btn btn-danger btn-cons">same width</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        <h5>Button Style</h5>
                      </div>
                      <div className="separator"></div>

                      <div className="card-content">
                        <p><code>.btn</code> class also can be styled with <code>.btn-[color]-outline</code></p>
                        <div className="button-row">
                          <button className="btn btn-primary-outline">primary</button>
                          <button className="btn btn-success-outline">success</button>
                          <button className="btn btn-warning-outline">warning</button>
                          <button className="btn btn-danger-outline">danger</button>
                          <button className="btn btn-info-outline">info</button>
                        </div>
                        <div className="separator separator-dashed"></div>

                        <p>outline style class can be reversed with <code>.btn-[color]-hover-outline</code></p>
                        <div className="button-row">
                          <button className="btn btn-primary-hover-outline">primary</button>
                          <button className="btn btn-success-hover-outline">success</button>
                          <button className="btn btn-warning-hover-outline">warning</button>
                          <button className="btn btn-danger-hover-outline">danger</button>
                          <button className="btn btn-info-hover-outline">info</button>
                        </div>
                        <div className="separator separator-dashed"></div>

                        <p><code>.btn</code> can use <code>.btn-rad</code> for rounded style button</p>
                        <div className="button-row">
                          <button className="btn btn-primary btn-rad">primary</button>
                          <button className="btn btn-success btn-rad">success</button>
                          <button className="btn btn-warning btn-rad">warning</button>
                          <button className="btn btn-danger btn-rad">danger</button>
                          <button className="btn btn-info btn-rad">info</button>
                        </div>
                        <div className="separator separator-dashed"></div>

                        <p>you can even do hover elevate effect with <code>.btn</code> used with <code>.btn-elevate</code></p>
                        <div className="button-row">
                          <button className="btn btn-primary btn-elevate">primary</button>
                          <button className="btn btn-success btn-elevate">success</button>
                          <button className="btn btn-warning btn-elevate">warning</button>
                          <button className="btn btn-danger btn-elevate">danger</button>
                          <button className="btn btn-info btn-elevate">info</button>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        <h5>Button group</h5>
                      </div>
                      <div className="separator"></div>

                      <div className="card-content">
                        <p><code>.btn-group</code> used to combine a few a button into stick to each other, <code>.btn-group</code> > <code>.btn</code></p>
                        <div className="button-row">
                          <div className="btn-group">
                            <button className="btn btn-primary">left</button>
                            <button className="btn btn-primary">middle</button>
                            <button className="btn btn-primary">right</button>
                          </div>
                        </div>
                        <div className="separator separator-dashed"></div>

                        <p><code>.btn</code> inside <code>.btn-group</code> can be styled with general <code>.btn</code> classes</p>
                        <div className="button-row">
                          <div className="btn-group">
                            <button className="btn btn-success-outline">left</button>
                            <button className="btn btn-success-outline">middle</button>
                            <button className="btn btn-success-outline">right</button>
                          </div>
                        </div>
                        <div className="separator separator-dashed"></div>

                        <p><code>.btn</code> inside <code>.btn-group</code> can be styled with general <code>.btn</code> classes</p>
                        <div className="button-row">
                          <div className="btn-group">
                            <button className="btn btn-info-outline"><i className="fas fa-info-circle"></i>info</button>
                            <button className="btn btn-info-outline"><i className="far fa-edit"></i>edit</button>
                            <button className="btn btn-info-outline"><i class="far fa-trash-alt"></i>delete</button>
                          </div>
                        </div>
                        <div className="separator separator-dashed"></div>

                        <p><code>.btn</code> can be turn into dropdown toogle by adding <code>.dropdown-toggle</code> as this code below. also can be customize by showing <code>.from-top</code> or <code>from-bottom</code></p>
                        <p>
                          <code>
                            &lt;div className="btn-group"&gt; <br />
                              &nbsp;&nbsp; &lt;button className="btn btn-secondary dropdown-toggle"&gt;dropdown  &lt;/button&gt; <br />
                              &nbsp;&nbsp; &lt;div className="dropdown-menu from-top"&gt; <br />
                                &nbsp;&nbsp;&nbsp;&nbsp; &lt;a href="" className="dropdown-item"&gt;dropdown link  &lt;/a&gt; <br />
                                &nbsp;&nbsp;&nbsp;&nbsp; &lt;a href="" className="dropdown-item"&gt;dropdown link  &lt;/a&gt; <br />
                                &nbsp;&nbsp;&nbsp;&nbsp; &lt;a href="" className="dropdown-item"&gt;dropdown link  &lt;/a&gt; <br />
                                &nbsp;&nbsp;&nbsp;&nbsp; &lt;a href="" className="dropdown-item"&gt;dropdown link  &lt;/a&gt; <br />
                              &nbsp;&nbsp; &lt;/div&gt; <br />
                            &lt;/div&gt;
                          </code>
                        </p>
                        <div className="button-row">
                          <div className="btn-group">
                            <button className="btn btn-secondary dropdown-toggle">dropdown</button>
                            <div className="dropdown-menu from-top">
                              <a href="" className="dropdown-item">dropdown link</a>
                              <a href="" className="dropdown-item">dropdown link</a>
                              <a href="" className="dropdown-item">dropdown link</a>
                              <a href="" className="dropdown-item">dropdown link</a>
                            </div>
                          </div>
                        </div>
                        <div className="separator separator-dashed"></div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

$(document).ready(function() {
  $('.dropdown-toggle').click(function() {
    $('.dropdown-menu').toggleClass('show');
  })

  $(document).mouseup(function (e)
                    {
    var container = $(".dropdown-menu"); // YOUR CONTAINER SELECTOR

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
      container.removeClass('show');
    }
  });
})

export default ButtonScreen;