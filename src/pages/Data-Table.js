// import React from "react";
import React, { Component } from "react";

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MemberData: [
        {id: 1, firstName: 'glantino', lastName: 'putra', username: '@glantino'},
        {id: 2, firstName: 'glantino', lastName: 'putra', username: '@glantino'},
        {id: 3, firstName: 'glantino', lastName: 'putra', username: '@glantino'},
        {id: 4, firstName: 'alexandro', lastName: 'putra', username: '@alexandro'},
        {id: 5, firstName: 'alexandro', lastName: 'putra', username: '@alexandro'},
        {id: 6, firstName: 'alexandro', lastName: 'putra', username: '@alexandro'},
        {id: 7, firstName: 'fadli', lastName: 'pratama', username: '@fadli'},
        {id: 8, firstName: 'fadli', lastName: 'pratama', username: '@fadli'},
        {id: 9, firstName: 'fadli', lastName: 'pratama', username: '@fadli'},
        {id: 10, firstName: 'zacky', lastName: 'zicko', username: '@zacky'},
        {id: 11, firstName: 'zacky', lastName: 'zicko', username: '@zacky'},
        {id: 12, firstName: 'zacky', lastName: 'zicko', username: '@zacky'},
        {id: 13, firstName: 'gladystia', lastName: 'bella', username: '@gladystia'},
        {id: 14, firstName: 'gladystia', lastName: 'bella', username: '@gladystia'},
        {id: 15, firstName: 'gladystia', lastName: 'bella', username: '@gladystia'},
        {id: 16, firstName: 'ara', lastName: 'raihana', username: '@ara'},
        {id: 17, firstName: 'ara', lastName: 'raihana', username: '@ara'},
        {id: 18, firstName: 'ara', lastName: 'raihana', username: '@ara'},
        {id: 19, firstName: 'ade', lastName: 'tetew', username: '@ade'},
        {id: 20, firstName: 'ade', lastName: 'tetew', username: '@ade'},
        {id: 21, firstName: 'ade', lastName: 'tetew', username: '@ade'},
        {id: 22, firstName: 'ita', lastName: 'yanti', username: '@ita'},
        {id: 23, firstName: 'ita', lastName: 'yanti', username: '@ita'},
        {id: 24, firstName: 'ita', lastName: 'yanti', username: '@ita'},
        {id: 25, firstName: 'aray', lastName: 'andreas', username: '@aray'},
        {id: 26, firstName: 'aray', lastName: 'andreas', username: '@aray'},
        {id: 27, firstName: 'aray', lastName: 'andreas', username: '@aray'},
        {id: 28, firstName: 'muarif', lastName: 'rif', username: '@muarif'},
        {id: 29, firstName: 'muarif', lastName: 'rif', username: '@muarif'},
        {id: 30, firstName: 'muarif', lastName: 'rif', username: '@muarif'},
      ]
    }
  }

  render() {

    const items = this.state.MemberData.map((item, key) =>
      <tr id={item.id}>
        <th>{item.id}</th>
        <td>{item.firstName}</td>
        <td>{item.lastName}</td>
        <td>{item.username}</td>
      </tr>
    );

    return (
      <div className="right-panel">
        <div className="p-20-c">
          <div className="breadcrumb">
            <div className="page-title">Data Table</div>
            <div className="page-info">
              <div className="breadcrumb-item">UI Elements</div>
              <div className="breadcrumb-item">Data Table</div>
            </div>
          </div>
        </div>
        <div className="p-20-c">
          <div className="row" id="index-first-row">
            <div className="col-12">
              <div className="card">
                <div className="card-body">
                  <div className="card-title">
                    option #1
                  </div>
                  <div className="card-content">
                    <p></p>
                    <div className="dataTable-wrapper">
                      <div className="row">
                        <div className="col-6">
                          <div className="dataTable-EntriesOpt">
                            <label>show</label>
                            <div className="form-group form-group-default">
                              <select className="form-control">
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="50">50</option>
                              </select>
                            </div>
                            <label>entries</label>
                          </div>
                        </div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-6">
                              <div class="input-group">
                                <input type="text" placeholder="search" class="form-control" id="search" />
                                <div class="input-group-append">
                                  <button class="btn btn-success btn-icon ">
                                    <i class="fas fa-search"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                            <div className="col-6 d-flex">
                              <div className="btn-group ml-auto">
                                <button className="btn btn-info"><i className="fas fa-download"></i>import</button>
                                <button className="btn btn-success"><i className="fas fa-plus"></i>add module</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12">
                          <table className="table add-pagination table-bordered" id="table-1">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>first name</th>
                                <th>last name</th>
                                <th>username</th>
                              </tr>
                            </thead>
                            <tbody>
                              {items}
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-5">
                          <div className="dataTable-info">
                            <h4></h4>
                          </div>
                        </div>
                        <div className="col-7">
                          <div className="dataTable-paginate">
                            <div className="pagination" >
                              <ol id="wrapper"></ol>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Table;
