// import React from "react";
import React, { Component, useState } from "react";
import $ from "jquery";
import dropify from "dropify";
import "../../node_modules/dropify/src/sass/dropify.scss";
import Input from "../components/Input";

function handleInput(phrase) {
  alert(phrase);
}


const Form = () => {
    const [phrase, setPhrase] = useState('');

    return (
      <div className="right-panel">
        <div className="p-20-c">
          <div className="breadcrumb">
            <div className="page-title">Form</div>
            <div className="page-info">
              <div className="breadcrumb-item">UI Elements</div>
              <div className="breadcrumb-item">Form</div>
            </div>
          </div>
        </div>
        <div className="p-20-c">
          <div className="row" id="index-first-row">
            <div className="col-7">
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #1
                      </div>
                      <h2 className="m-20-vc">Form modern style</h2>
                      <div className="card-content">
                        <Input type='modern' />
                      </div>
                    </div>
                  </div>
                </div>
              </div>  
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #4
                      </div>
                      <h2 className="m-10-vc">Input group</h2>
                      <div className="card-content">                  

                        <div className="separator separator-dashed"></div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #6
                      </div>
                      <h2 className="m-10-vc">File input</h2>
                      <div className="card-content">

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #8
                      </div>
                      <h2 className="m-10-vc">Select</h2>
                      <div className="card-content">
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #10
                      </div>
                      <h2 className="m-10-vc">File upload @ Dropify</h2>
                      <div className="card-content">
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-5">
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #2
                      </div>
                      <h2 className="m-10-vc">Form default style</h2>
                      <div className="card-content">
                        <Input />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #3
                      </div>
                      <h2 className="m-10-vc">Option control</h2>
                      <div className="card-content">
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #5
                      </div>
                      <h2 className="m-10-vc">Swicth control</h2>
                      <div className="card-content">
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #7
                      </div>
                      <h2 className="m-10-vc">Checkbox</h2>
                      <div className="card-content">
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #9
                      </div>
                      <h2 className="m-10-vc">Datepicker</h2>
                      <div className="card-content">
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
}

$(document).ready(function() {

  
})

export default Form;