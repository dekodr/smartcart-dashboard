// import React from "react";
import React, { Component } from "react";

class Typography extends Component {
  render() {
    return (
      <div className="right-panel">
        <div className="p-20-c">
          <div className="breadcrumb">
            <div className="page-title">Folder</div>
            <div className="page-info">
              <div className="breadcrumb-item">UI Elements</div>
              <div className="breadcrumb-item">Folder</div>
            </div>
          </div>
        </div>
        <div className="p-20-c">
          <div className="row" id="index-first-row">
            <div className="col-12">
              <div className="card">
                <div className="card-body">
                  <div className="card-title">
                    <h3>List Member</h3>
                  </div>
                  <div className="card-content folder">
                    <div className="row">
                      <div className="col-4">
                        <div className="card">
                          <div className="image">
                            <img src={require("../asset/images/icon-file.png")} />
                            <img src={require("../asset/images/user-profile.png")} class="profile" />
                          </div>
                          <div className="content">
                            <div className="name">
                              Muarif Gustiar
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="card">
                          <div className="image">
                            <img src={require("../asset/images/icon-file.png")} />
                            <img src={require("../asset/images/user-profile.png")} class="profile" />
                          </div>
                          <div className="content">
                            <div className="name">
                              Muarif Gustiar
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="card">
                          <div className="image">
                            <img src={require("../asset/images/icon-file.png")} />
                            <img src={require("../asset/images/user-profile.png")} class="profile" />
                          </div>
                          <div className="content">
                            <div className="name">
                              Muarif Gustiar
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="card">
                          <div className="image">
                            <img src={require("../asset/images/icon-file.png")} />
                            <img src={require("../asset/images/user-profile.png")} class="profile" />
                          </div>
                          <div className="content">
                            <div className="name">
                              Muarif Gustiar
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="card">
                          <div className="image">
                            <img src={require("../asset/images/icon-file.png")} />
                            <img src={require("../asset/images/user-profile.png")} class="profile" />
                          </div>
                          <div className="content">
                            <div className="name">
                              Muarif Gustiar
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Typography;
