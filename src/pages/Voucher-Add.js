// import React from "react";
import React, { Component } from "react";
import dropify from "dropify";
import "../../node_modules/dropify/src/sass/dropify.scss";

class Typography extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MemberData: [
        {id: 35, Code: '19450817012', Campaign: 'Buy 1 get 1', Status: 'active', Created: '2-2-2020', Redemption: '17'},
      ]
    }
  }

  render() {

    const items = this.state.MemberData.map((item, key) =>
      <tr id={item.id}>
        <th>{item.Code}</th>
        <td>{item.Campaign}</td>
        <td>
          <button className="btn btn-success btn-xs">{item.Status}</button>
        </td>
        <td>{item.Created}</td>
        <td>{item.Redemption}</td>
        <td>
          <div className="button-row">
            <button className="btn btn-primary btn-icon btn-rad btn-icon-sm"><i class="fas fa-qrcode"></i></button>
            <button className="btn btn-danger btn-icon btn-rad btn-icon-sm"><i className="fas fa-trash"></i></button>
          </div>
        </td>
      </tr>
    );

    return (
      <div className="right-panel">
        <div className="p-20-c">
          <div className="breadcrumb">
            <div className="page-title">Add Voucher</div>
            <div className="page-info">
              <div className="breadcrumb-item">Voucher</div>
              <div className="breadcrumb-item">Add Voucher</div>
            </div>
          </div>
        </div>
        <div className="p-20-c">
          <div className="row" id="index-first-row">
            <div className="col-12">
              <div className="card">
                <div className="card-body">
                  <div className="card-title">
                    
                  </div>
                  <div className="card-content">
                    <div className="row">
                      <div className="col-12">
                        <form action="#" className="form">
                          <div className="form-group">
                            <label htmlFor="#" className="form-label">campaign name</label>
                            <input type="text" className="form-control" />
                          </div>
                          <div className="row">
                            <div className="col-6">
                              <div className="form-group">
                                <label htmlFor="#" className="form-label">tanggal dimulai</label>
                                <input type="date" className="form-control" />
                              </div>
                            </div>
                            <div className="col-6">
                              <div className="form-group">
                                <label htmlFor="#" className="form-label">tanggal berakhir</label>
                                <input type="date" className="form-control" />
                              </div>
                            </div>
                          </div>
                          <div className="button-group d-flex">
                            <button className="btn btn-success ml-auto">submit</button>
                          </div>
                        </form>
                      </div>
                      <div className="col-4">
                        <form action="#" className="form">
                          <div className="form-group form-group-default">
                            <input type="file" class="dropify" data-height="175" />
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="card">
                <div className="card-body">
                  <div className="card-title">
                    <div className="row">
                      <div className="col-3 d-flex align-item-center">
                        <h3>Product</h3>
                      </div>
                      <div className="col-9 d-flex">
                        <button className="btn btn-info ml-auto"><i class="fas fa-file"></i>Import</button>
                      </div>
                    </div>
                  </div>
                  <div className="card-content">
                    <table className="table">
                      <thead>
                        <tr>
                          <th>
                            <form action="#" class="form">
                            <div className="checkbox-control">
                              <input type="checkbox" id="checkbox-1" />
                              <label for="checkbox-1">SKU</label>
                            </div>
                          </form>
                          </th>
                          <th>Name</th>
                        </tr>
                      </thead>
                      <tbody>
                        <td>
                          <form action="#" class="form">
                            <div className="checkbox-control">
                              <input type="checkbox" id="checkbox-2" />
                              <label for="checkbox-2">0090201829</label>
                            </div>
                          </form>
                        </td>
                        <td>Tango 120gr</td>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Typography;
