// import React from "react";
import React, { Component, setState, state } from "react";
import $ from "jquery";

export default function Form() {
  const [state, setState] = React.useState({
    username: null,
    level: "agree",
  });

  function handleChange(evt) {
    const id = evt.target.id;
    const value =
      evt.target.type === "checkbox" ? evt.target.checked : evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value
    });
    console.log(id+':'+value);
  }

  function handleSubmit(evt) {
    
  }

  return (
    <div className="right-panel">
      <div className="p-20-c">
        <div className="breadcrumb">
          <div className="page-title">Form</div>
          <div className="page-info">
            <div className="breadcrumb-item">UI Elements</div>
            <div className="breadcrumb-item">Form</div>
          </div>
        </div>
      </div>
      <div className="p-20-c">
        <div className="row" id="index-first-row">
          <div className="col-7">
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-body">
                    <div className="card-title">
                      option #1
                    </div>
                    <h2 className="m-20-vc">Pages modern style</h2>
                    <div className="card-content">
                      <p><code>.form</code> is used to wrap <code>.form-group</code> and <code>.radio</code>, as the example code below</p>
                      <p>
                        <code>
                          &lt;div className="form-group"&gt; <br/>  
                            &nbsp;&nbsp; &lt;label htmlFor="#" className="form-label"&gt;username&lt;/label&gt; <br/>  
                            &nbsp;&nbsp; &lt;input type="text" className="form-control" /&gt; <br/>  
                          &lt;/div&gt; <br/>  
                        </code>
                      </p>
                      <div action="#" className="form">
                        <div className="form-group">
                          <label htmlFor="#" className="form-label">username</label>
                          <input id="username" type="text" className="form-control" onChange={handleChange} value={state.username} />
                        </div>
                        <div className="row">
                          <div className="col-6">
                            <div className="form-group">
                              <label htmlFor="#" className="form-label">first name</label>
                              <input id="first-name" type="text" className="form-control" onChange={handleChange} />
                            </div>
                          </div>
                          <div className="col-6">
                            <div className="form-group">
                              <label htmlFor="#" className="form-label">last name</label>
                              <span className="pull-right text-danger">*</span> 
                              <input id="last-name" type="text" className="form-control" onChange={handleChange} />
                            </div>
                          </div>
                        </div>
                        <div className="form-group">
                          <label htmlFor="#" className="form-label">your email</label>
                          <span className="pull-right text-danger">*</span> 
                          <input id="email" type="email" className="form-control" placeholder="ex: your.email@gmail.com" onChange={handleChange} />
                        </div>

                        <div className="separator separator-dashed"></div>
                        <p><code>.form-group</code> coming with one additional state class, for making it on a disabled state, simply add <code>.disabled</code> class</p>
                        <div className="form-group disabled">
                          <label htmlFor="#" className="form-label">disabled</label>
                          <span className="pull-right text-danger">*</span> 
                          <input type="text" className="form-control" placeholder="you can't put input here" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>  
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-body">
                    <div className="card-title">
                      option #4
                    </div>
                    <h2 className="m-10-vc">Input group</h2>
                    <div className="card-content">
                      <p>use <code>.input-group</code> for input with icon</p>
                      <p>
                        <code>
                          &lt;div className="input-group"&gt; <br/>
                            &nbsp;&nbsp;&nbsp; &lt;div className="input-group-prepend"&gt; <br/>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;div className="input-group-icon"&gt; <br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;i className="far fa-calendar-alt"&gt;&lt;/i&gt; <br/>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/div&gt; <br/>
                            &nbsp;&nbsp;&nbsp; &lt;/div&gt; <br/>
                            &nbsp;&nbsp;&nbsp; &lt;input type="text" placeholder="Username" className="form-control" /&gt; <br/>
                          &lt;/div&gt;
                        </code>
                      </p>
                      <form action="#" className="form">
                        <div className="input-group">
                          <div className="input-group-prepend">
                            <div className="input-group-icon">
                              <i className="far fa-calendar-alt"></i>
                            </div>
                          </div>
                          <input type="text" placeholder="Username" className="form-control" />
                        </div>
                      </form>

                      <div className="separator separator-dashed"></div>

                      <p><code>.input-group</code> can be used with <code>.btn</code> as this example below</p>
                      <p>
                        <code>
                          &lt;div className="input-group"&gt; <br/>
                            &nbsp;&nbsp;&nbsp;&lt;input type="email" placeholder="email" className="form-control" /&gt; <br/>
                            &nbsp;&nbsp;&nbsp;&lt;div className="input-group-append"&gt; <br/>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;button className="btn btn-success"&gt;submit&lt;/button&gt; <br/>
                            &nbsp;&nbsp;&nbsp;&lt;/div&gt; <br/>
                          &lt;/div&gt;
                        </code>
                      </p>
                      <form action="#" className="form">
                        <div className="input-group">
                          <input type="email" placeholder="email" className="form-control" />
                          <div className="input-group-append">
                            <button className="btn btn-success">submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-body">
                    <div className="card-title">
                      option #6
                    </div>
                    <h2 className="m-10-vc">File input</h2>
                    <div className="card-content">
                      <p>only come with this, no additional class.</p>
                      <p>
                        <code>
                          &lt;div className="file-upload"&gt; <br/>
                            &nbsp;&nbsp;&lt;div className="file-select"&gt; <br/>
                              &nbsp;&nbsp;&nbsp;&nbsp;&lt;div className="file-select-name" id="noFile"&gt;No file chosen...&lt;/div&gt; <br/> 
                              &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type="file" name="chooseFile" id="chooseFile" /&gt; <br/>
                              &nbsp;&nbsp;&nbsp;&nbsp;&lt;div className="file-select-button" id="fileName"&gt;Browse&lt;/div&gt; <br/>
                            &nbsp;&nbsp;&lt;/div&gt; <br/>
                          &lt;/div&gt;
                        </code>
                      </p>
                      <form action="#" className="form">
                        <div className="file-upload">
                          <div className="file-select">
                            <div className="file-select-name" id="noFile">No file chosen...</div> 
                            <input type="file" name="chooseFile" id="chooseFile" />
                            <div className="file-select-button" id="fileName">Browse</div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-body">
                    <div className="card-title">
                      option #8
                    </div>
                    <h2 className="m-10-vc">Select</h2>
                    <div className="card-content">
                      <p>only come with this, no additional class.</p>
                      <p>
                        <code>
                          &lt;div className="form-group form-group-default"&gt; <br/>
                            &nbsp;&nbsp;&lt;select className="form-control"&gt; <br/>
                              &nbsp;&nbsp;&nbsp;&nbsp;&lt;option value="Select"&gt;Select&lt;/option&gt; <br/>
                            &nbsp;&nbsp;&lt;/select&gt; <br/>
                          &lt;/div&gt;
                        </code>
                      </p>
                      <form action="#" className="form">
                        <div className="form-group form-group-default">
                          <select className="form-control">
                            <option value="Select">Select</option>
                          </select>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-body">
                    <div className="card-title">
                      option #10
                    </div>
                    <h2 className="m-10-vc">File upload @ Dropify</h2>
                    <div className="card-content">
                      <p>You need to import dropify at the page if you want to use this component</p>
                      <p>
                        <code>
                          &lt;input type="file" className="dropify" data-height="175" /&gt;
                        </code>
                      </p>
                      <form action="#" className="form">
                        <div className="form-group form-group-default">
                          <input type="file" className="dropify" data-height="175" />
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-5">
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-body">
                    <div className="card-title">
                      option #2
                    </div>
                    <h2 className="m-10-vc">Pages default style</h2>
                    <div className="card-content">
                      <p>add <code>.form-group-default</code> to <code>.form-group</code> for default style. <code>.form-group-default</code> come with hint text that you can use by add it inside <code>.form-group</code></p>
                      <p>
                        <code>&lt;span className="hint-text"&gt;e.g. "Mona Lisa Potrait"&lt;/span&gt; </code>
                      </p>
                      <form action="#" className="form">
                        <div className="form-group form-group-default">
                          <label htmlFor="#" className="form-label">username</label>
                          <span className="hint-text">e.g. "Mona Lisa Potrait"</span> 
                          <input type="text" className="form-control" />
                        </div>
                        <div className="form-group form-group-default">
                          <label htmlFor="#" className="form-label">password</label>
                          <span className="hint-text">e.g. "Mona Lisa Potrait"</span> 
                          <input type="password" className="form-control" />
                        </div>
                        <div className="form-group form-group-default">
                          <label htmlFor="#" className="form-label">email</label>
                          <span className="hint-text">ex: your.email@gmail.com</span> 
                          <input type="email" className="form-control" />
                        </div>
                        <div className="form-group form-group-default disabled">
                          <label htmlFor="#" className="form-label">disabled</label>
                          <span className="hint-text"></span> 
                          <input type="email" className="form-control" placeholder="you can't put input here" />
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-body">
                    <div className="card-title">
                      option #3
                    </div>
                    <h2 className="m-10-vc">Option control</h2>
                    <div className="card-content">
                      <p>use <code>.radio</code> for radio button, it's also come in color classes such <code>.radio-success</code></p>
                      <p>
                        <code>
                          &lt;div className="radio radio-success"&gt; <br/>
                            &nbsp;&nbsp; &lt;input type="radio" id="radio1" /&gt; <br/>
                            &nbsp;&nbsp; &lt;label id="radio-label1" className="radio-label"&gt;agree&lt;/label&gt; <br/>
                            &nbsp;&nbsp; &lt;input type="radio" id="radio2" /&gt; <br/>
                            &nbsp;&nbsp; &lt;label id="radio-label2" className="radio-label"&gt;disagree&lt;/label&gt; <br/>
                          &lt;/div&gt;
                        </code>
                      </p>
                      <form action="#" className="form">
                        <div className="radio radio-success">
                          <input 
                            type="radio" 
                            id="radio1"
                            value="agree"
                            checked={state.level === "agree"}
                            onChange={handleChange} />
                          <label id="radio-label1" htmlFor="yes" className="radio-label">agree</label>
                          <input 
                            type="radio" 
                            id="radio2" 
                            value="disagree"
                            checked={state.level === "disagree"}
                            onChange={handleChange} />
                          <label id="radio-label2" htmlFor="no" className="radio-label">disagree</label>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-body">
                    <div className="card-title">
                      option #5
                    </div>
                    <h2 className="m-10-vc">Swicth control</h2>
                    <div className="card-content">
                      <p>custom checkbox but uses the <code>.switch-control</code> to render a toggle switch, Switches also support the <code>disabled</code> attribute</p>
                      <p>
                        <code>
                          &lt;div className="switch-control"&gt; <br/>
                            &nbsp;&nbsp;&lt;label className="switch"&gt; <br/>
                              &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type="checkbox" /&gt; <br/>
                              &nbsp;&nbsp;&nbsp;&nbsp;&lt;span className="slider round"&gt;&lt;/span&gt; <br/>
                            &nbsp;&nbsp;&lt;/label&gt; <br/>
                            &nbsp;&nbsp;&lt;label className="switch-label"&gt;Toggle this&lt;/label&gt; <br/>
                          &lt;/div&gt;
                        </code>
                      </p>
                      <form action="#" className="form">
                        <div className="row">
                          <div className="col-6">
                            <div className="switch-control switch-success">
                              <label className="switch">
                                <input type="checkbox" />
                                <span className="slider"></span>
                              </label>
                              <label className="switch-label">Toggle this swicth element</label>
                            </div>
                            <div className="switch-control">
                              <label className="switch">
                                <input type="checkbox" disabled="disabled" />
                                <span className="slider"></span>
                              </label>
                              <label className="switch-label">Disabled switch</label>
                            </div>
                          </div>
                          <div className="col-6">
                            <div className="switch-control switch-primary">
                              <label className="switch">
                                <input type="checkbox" />
                                <span className="slider"></span>
                              </label>
                              <label className="switch-label">Primary</label>
                            </div>
                            <div className="switch-control switch-success">
                              <label className="switch">
                                <input type="checkbox" />
                                <span className="slider"></span>
                              </label>
                              <label className="switch-label">Success</label>
                            </div>
                            <div className="switch-control switch-warning">
                              <label className="switch">
                                <input type="checkbox" />
                                <span className="slider"></span>
                              </label>
                              <label className="switch-label">Warning</label>
                            </div>
                            <div className="switch-control switch-danger">
                              <label className="switch">
                                <input type="checkbox" />
                                <span className="slider"></span>
                              </label>
                              <label className="switch-label">Danger</label>
                            </div>
                            <div className="switch-control switch-info">
                              <label className="switch">
                                <input type="checkbox" />
                                <span className="slider"></span>
                              </label>
                              <label className="switch-label">Info</label>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-body">
                    <div className="card-title">
                      option #7
                    </div>
                    <h2 className="m-10-vc">Checkbox</h2>
                    <div className="card-content">
                      <p>checkbox use the <code>.checkbox-control</code> to render, checkbox also support the <code>disabled</code> attribute</p>
                      <p>
                        <code>
                          &lt;div className="checkbox-control"&gt; <br/>
                            &nbsp;&nbsp;&lt;input type="checkbox" id="checkbox-1" /&gt; <br/>
                            &nbsp;&nbsp;&lt;label htmlFor="checkbox-1"&gt;checked&lt;/label&gt; <br/>
                          &lt;/div&gt;
                        </code>
                      </p>
                      <form action="#" className="form">
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor="#" className="form-label">Checkboxes</label>
                          </div>
                          <div className="col-8">
                            <div className="checkbox-control">
                              <input type="checkbox" id="checkbox-1" />
                              <label htmlFor="checkbox-1">checked</label>
                            </div>
                            <div className="checkbox-control">
                              <input type="checkbox" id="checkbox-2" disabled="disabled" />
                              <label htmlFor="checkbox-2">Disabled</label>
                            </div>
                            <div className="checkbox-control">
                              <input type="checkbox" id="checkbox-3" disabled="disabled" checked="checked" />
                              <label htmlFor="checkbox-3">Disabled checked</label>
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-4">
                            <label htmlFor="#" className="form-label">Checkboxes inline</label>
                          </div>
                          <div className="col-8">
                            <div className="form-check-inline">
                              <div className="checkbox-control">
                                <input type="checkbox" id="checkbox-4" />
                                <label htmlFor="checkbox-4">HTML</label>
                              </div>
                            </div>
                            <div className="form-check-inline">
                              <div className="checkbox-control">
                                <input type="checkbox" id="checkbox-5" disabled="disabled" />
                                <label htmlFor="checkbox-5">CSS</label>
                              </div>
                            </div>
                            <div className="form-check-inline">
                              <div className="checkbox-control">
                                <input type="checkbox" id="checkbox-6" disabled="disabled" checked="checked" />
                                <label htmlFor="checkbox-6">Javascript</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="card">
                  <div className="card-body">
                    <div className="card-title">
                      option #9
                    </div>
                    <h2 className="m-10-vc">Datepicker</h2>
                    <div className="card-content">
                      <p>only come with this, no additional class.</p>
                      <p>
                        <code>
                          &lt;div className="form-group form-group-default"&gt; <br/>
                            &nbsp;&nbsp;&lt;input type="date" className="form-control" /&gt; <br/>
                          &lt;/div&gt;
                        </code>
                      </p>
                      <form action="#" className="form">
                        <div className="form-group form-group-default">
                          <input type="date" className="form-control" />
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

$(document).ready(function() {

  
})
