// import React from "react";
import React, { Component } from "react";

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    }
  }

  render() {

    return (
      <div className="right-panel">
        <div className="p-20-c">
          <div className="breadcrumb">
            <div className="page-title">Tables</div>
            <div className="page-info">
              <div className="breadcrumb-item">UI Elements</div>
              <div className="breadcrumb-item">Tables</div>
            </div>
          </div>
        </div>
        <div className="p-20-c">
          <div className="row" id="index-first-row">
            <div className="col-6">
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #1
                      </div>
                      <div className="card-content">
                        <p></p>
                        <table className="table" id="table-1">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>first name</th>
                              <th>last name</th>
                              <th>username</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th>1</th>
                              <td>Alexandro</td>
                              <td>Putra</td>
                              <td>@alexandro</td>
                            </tr>
                            <tr>
                              <th>2</th>
                              <td>Glantino</td>
                              <td>Putra</td>
                              <td>@glantino</td>
                            </tr>
                            <tr>
                              <th>3</th>
                              <td>Fadli</td>
                              <td>the bird</td>
                              <td>@fadli</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #3
                      </div>
                      <div className="card-content">
                        <p></p>
                        <table className="table">
                          <thead className="thead-green">
                            <tr>
                              <th>#</th>
                              <th>first name</th>
                              <th>last name</th>
                              <th>username</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th>1</th>
                              <td>Alexandro</td>
                              <td>Putra</td>
                              <td>@alexandro</td>
                            </tr>
                            <tr>
                              <th>2</th>
                              <td>Glantino</td>
                              <td>Putra</td>
                              <td>@glantino</td>
                            </tr>
                            <tr>
                              <th>3</th>
                              <td>Fadli</td>
                              <td>the bird</td>
                              <td>@fadli</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #5
                      </div>
                      <div className="card-content">
                        <p></p>
                        <table className="table table-hover">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>first name</th>
                              <th>last name</th>
                              <th>username</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th>1</th>
                              <td>Alexandro</td>
                              <td>Putra</td>
                              <td>@alexandro</td>
                            </tr>
                            <tr>
                              <th>2</th>
                              <td>Glantino</td>
                              <td>Putra</td>
                              <td>@glantino</td>
                            </tr>
                            <tr>
                              <th>3</th>
                              <td>Fadli</td>
                              <td>the bird</td>
                              <td>@fadli</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-6">
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #2
                      </div>
                      <div className="card-content">
                        <p></p>
                        <table className="table table-bordered" id="table-2">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>first name</th>
                              <th>last name</th>
                              <th>username</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th>1</th>
                              <td>Alexandro</td>
                              <td>Putra</td>
                              <td>@alexandro</td>
                            </tr>
                            <tr>
                              <th>2</th>
                              <td>Glantino</td>
                              <td>Putra</td>
                              <td>@glantino</td>
                            </tr>
                            <tr>
                              <th>3</th>
                              <td>Fadli</td>
                              <td>the bird</td>
                              <td>@fadli</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #4
                      </div>
                      <div className="card-content">
                        <p></p>
                        <table className="table table-striped">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>first name</th>
                              <th>last name</th>
                              <th>username</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th>1</th>
                              <td>Alexandro</td>
                              <td>Putra</td>
                              <td>@alexandro</td>
                            </tr>
                            <tr>
                              <th>2</th>
                              <td>Glantino</td>
                              <td>Putra</td>
                              <td>@glantino</td>
                            </tr>
                            <tr>
                              <th>3</th>
                              <td>Fadli</td>
                              <td>the bird</td>
                              <td>@fadli</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Table;
