// import React from "react";
import React, { Component } from "react";
import $ from "jquery";

class Tabs extends Component {
  render() {
    return (
      <div className="right-panel">
        <div className="p-20-c">
          <div className="breadcrumb">
            <div className="page-title">Tabs & Accordion</div>
            <div className="page-info">
              <div className="breadcrumb-item">UI Elements</div>
              <div className="breadcrumb-item">Tabs & Accordion</div>
            </div>
          </div>
        </div>
        <div className="p-20-c">
          <div className="row" id="index-first-row">
            <div className="col-6">
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #1
                      </div>
                      <h2 className="m-10-vc">Accordion</h2>
                      <div className="card-content">
                        <p>use <code>.accordion-group</code> as a wrapper of <code>.accordion</code></p>
                        <p>
                          <code>
                            &lt;div className="accordion"&gt; <br/>
                              &nbsp;&nbsp; &lt;div className="accordion-header all-caps" id="heading1"&gt; <br/>
                                &nbsp;&nbsp;&nbsp;&nbsp; accordion header <br/>
                                &nbsp;&nbsp;&nbsp;&nbsp; &lt;i className="pull-right fas fa-plus"&gt;&lt;/i&gt; <br/>
                              &nbsp;&nbsp; &lt;/div&gt; <br/>
                              &nbsp;&nbsp; &lt;div className="collapse" id="collapse1"&gt; <br/>
                                &nbsp;&nbsp;&nbsp;&nbsp; &lt;div className="accordion-body"&gt; <br/>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; your content here <br/>
                                &nbsp;&nbsp;&nbsp;&nbsp; &lt;/div&gt; <br/>
                              &nbsp;&nbsp; &lt;/div&gt; <br/>
                            &lt;/div&gt; <br/>
                          </code>
                        </p>
                        <div className="accordion-group">
                          <div className="accordion">
                            <div className="accordion-header all-caps" id="heading1">accordion header <i className="pull-right fas fa-plus"></i></div>
                            <div className="collapse" id="collapse1">
                              <div className="accordion-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eius cupiditate cum officiis, eum esse aperiam voluptatum. Molestias earum perferendis quibusdam similique magnam facere nostrum laboriosam ab rem? Ducimus, beatae?
                              </div>
                            </div>
                          </div>
                          <div className="accordion">
                            <div className="accordion-header all-caps" id="heading2">accordion header <i className="pull-right fas fa-plus"></i></div>
                            <div className="collapse" id="collapse2">
                              <div className="accordion-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eius cupiditate cum officiis, eum esse aperiam voluptatum. Molestias earum perferendis quibusdam similique magnam facere nostrum laboriosam ab rem? Ducimus, beatae?
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eius cupiditate cum officiis, eum esse aperiam voluptatum. Molestias earum perferendis quibusdam similique magnam facere nostrum laboriosam ab rem? Ducimus, beatae?
                              </div>
                            </div>
                          </div>
                          <div className="accordion">
                            <div className="accordion-header all-caps" id="heading3">accordion header <i className="pull-right fas fa-plus"></i></div>
                            <div className="collapse" id="collapse3">
                              <div className="accordion-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eius cupiditate cum officiis, eum esse aperiam voluptatum. Molestias earum perferendis quibusdam similique magnam facere nostrum laboriosam ab rem? Ducimus, beatae?
                              </div>
                            </div>
                          </div>
                          <div className="accordion">
                            <div className="accordion-header all-caps" id="heading1">accordion header <i className="pull-right fas fa-plus"></i></div>
                            <div className="collapse" id="collapse1">
                              <div className="accordion-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eius cupiditate cum officiis, eum esse aperiam voluptatum. Molestias earum perferendis quibusdam similique magnam facere nostrum laboriosam ab rem? Ducimus, beatae?
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-6">
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #2
                      </div>
                      <h2 className="m-10-vc">Tabs</h2>
                      <div className="card-content">
                        <p>use <code>.tabs-group</code> in purpose to use tab element</p>
                        <p>
                          <code>
                            &lt;ul class="nav-tabs"&gt; <br/>
                              &nbsp;&nbsp; &lt;li class="tab-item current" data-tab="tab-1"&gt;Tab One&lt;/li&gt; <br/>
                            &lt;/ul&gt; <br/>
                            &lt;div className="tab-content"&gt; <br/>
                              &nbsp;&nbsp; &lt;div id="tab-1" class="tab-pane current"&gt; <br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; your content here <br/>
                              &nbsp;&nbsp; &lt;/div&gt; <br/>
                            &lt;/div&gt;
                          </code>
                        </p>
                        <div className="tabs-group" id="tg-1">
                          <ul class="nav-tabs">
                            <li class="tab-item current" data-tab="tab-1">Tab One</li>
                            <li class="tab-item" data-tab="tab-2">Tab Two</li>
                            <li class="tab-item" data-tab="tab-3">Tab Three</li>
                            <li class="tab-item" data-tab="tab-4">Tab Four</li>
                          </ul>
                          <div className="tab-content">
                            <div id="tab-1" class="tab-pane current">
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                            <div id="tab-2" class="tab-pane">
                               Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </div>
                            <div id="tab-3" class="tab-pane">
                              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                            </div>
                            <div id="tab-4" class="tab-pane">
                              Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                          </div>
                        </div>

                        <div className="separator separator-dashed"></div>

                        <p><code>.tabs-group</code> come with orientation classes such <code>.tabs-left</code> or <code>.tabs-right</code></p>
                        <div className="tabs-group tabs-left" id="tg-2">
                          <ul class="nav-tabs">
                            <li class="tab-item current" data-tab="tab-1">Tab One</li>
                            <li class="tab-item" data-tab="tab-2">Tab Two</li>
                            <li class="tab-item" data-tab="tab-3">Tab Three</li>
                            <li class="tab-item" data-tab="tab-4">Tab Four</li>
                          </ul>
                          <div className="tab-content">
                            <div id="tab-1" class="tab-pane current">
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                            <div id="tab-2" class="tab-pane">
                               Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </div>
                            <div id="tab-3" class="tab-pane">
                              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                            </div>
                            <div id="tab-4" class="tab-pane">
                              Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                          </div>
                        </div>

                        <div className="separator separator-dashed"></div>

                        <p><code>.tabs-group</code> with <code>.tabs-right</code></p>
                        <div className="tabs-group tabs-right" id="tg-3">
                          <ul class="nav-tabs">
                            <li class="tab-item current" data-tab="tab-1">Tab One</li>
                            <li class="tab-item" data-tab="tab-2">Tab Two</li>
                            <li class="tab-item" data-tab="tab-3">Tab Three</li>
                            <li class="tab-item" data-tab="tab-4">Tab Four</li>
                          </ul>
                          <div className="tab-content">
                            <div id="tab-1" class="tab-pane current">
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                            <div id="tab-2" class="tab-pane">
                               Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </div>
                            <div id="tab-3" class="tab-pane">
                              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                            </div>
                            <div id="tab-4" class="tab-pane">
                              Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

$(document).ready(function() {

  $('.accordion-group').on('click', '.accordion .accordion-header', function(e) {
    e.preventDefault();
    $(this).next('.collapse').not(':animated').slideToggle();
    $(this).children('.fas').toggleClass('expanded');

    if ($(this).children('.fas').hasClass('expanded')) {
      $(this).children('.fas').removeClass('fa-plus').addClass('fa-minus');
    } else {
      $(this).children('.fas').removeClass('fa-minus').addClass('fa-plus');
    }
  });

  
  $('.tabs-group').mouseover(function() {
    var tab_group = $(this).attr('id');
    // console.log(tab_group);
    $('#'+tab_group+' ul.nav-tabs li').click(function(){
      var tab_id = $(this).attr('data-tab');

      $('#'+tab_group+' ul.nav-tabs li').removeClass('current');
      $('#'+tab_group+ ' .tab-pane').removeClass('current');

      $(this).addClass('current');
      $('#'+tab_group+" #"+tab_id).addClass('current');
    })
  })
  
})

export default Tabs;