// import React from "react";
import React, { Component } from "react";

class Typography extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MemberData: [
        {id: 35, NIP: '19450817 201908 1 012', nama: 'Sri Solihin Asih', region: 'DIY Yogyakarta', toko: 'Toko Makmur Maju Terus Mantul'},
        {id: 35, NIP: '19450817 201908 1 022', nama: 'Kamilia asikin', region: 'DKI Jakarta', toko: 'Toko Hijau Daun'},
        {id: 35, NIP: '19450817 201908 1 032', nama: 'Abdul Sajdah', region: 'Jawa Barat', toko: 'Toko Terompet Sangkakala'},
      ]
    }
  }

  render() {

    const items = this.state.MemberData.map((item, key) =>
      <tr id={item.id}>
        <th>{item.NIP}</th>
        <td>{item.nama}</td>
        <td>{item.region}</td>
        <td>{item.toko}</td>
        <td>
          <div className="button-row">
            <button className="btn btn-success btn-icon btn-rad btn-icon-sm"><i className="fas fa-edit"></i></button>
            <button className="btn btn-danger btn-icon btn-rad btn-icon-sm"><i className="fas fa-trash"></i></button>
          </div>
        </td>
      </tr>
    );

    return (
      <div className="right-panel">
        <div className="p-20-c">
          <div className="breadcrumb">
            <div className="page-title">Member</div>
            <div className="page-info">
              <div className="breadcrumb-item">Member</div>
            </div>
          </div>
        </div>
        <div className="p-20-c">
          <div className="row" id="index-first-row">
            <div className="col-12">
              <div className="card">
                <div className="card-body">
                  <div className="card-title">
                    <div className="row">
                      <div className="col-3 d-flex align-item-center">
                        <form action="#" className="form">
                          <div className="input-group">
                            <input type="email" placeholder="email" class="form-control" />
                            <div className="input-group-append">
                              <button className="btn btn-success btn-icon "><i className="fas fa-search"></i></button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div className="col-9 d-flex">
                        <button className="btn btn-success ml-auto"><i className="fas fa-plus"></i>Add New</button>
                        <button className="btn btn-info"><i class="fas fa-file"></i>Import</button>
                      </div>
                    </div>
                  </div>
                  <div className="card-content">
                    <table className="table">
                      <thead>
                        <tr>
                          <th>NIP</th>
                          <th>Nama</th>
                          <th>Region</th>
                          <th>Toko</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {items}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Typography;
