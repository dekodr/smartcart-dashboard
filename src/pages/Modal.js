// import React from "react";
import React, { Component } from "react";
import $ from "jquery";

class Modals extends Component {
  render() {
    return (
      <div className="right-panel">
        <div className="p-20-c">
          <div className="breadcrumb">
            <div className="page-title">Modals</div>
            <div className="page-info">
              <div className="breadcrumb-item">UI Elements</div>
              <div className="breadcrumb-item">Modals</div>
            </div>
          </div>
        </div>
        <div className="p-20-c">
          <div className="row" id="index-first-row">
            <div className="col-6">
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        option #1
                      </div>
                      <h2 className="m-10-vc">Modal</h2>
                      <div className="card-content">
                        <p>use <code>.accordion-group</code> as a wrapper of <code>.accordion</code></p>
                        <p>
                          <code>
                            &lt;div className="modal-wrapper"&gt; <br/>
                              &nbsp;&nbsp;&lt;span className="modal-outsider"&gt;&lt;/span&gt; <br/>
                              &nbsp;&nbsp;&lt;div className="modal-wrapper-inner"&gt; <br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;&lt;div className="modal"&gt; <br/>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;div className="modal-header"&gt; <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Modal Header <br/>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt; <br/>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;div className="modal-content"&gt; <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Modal Content <br/>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt; <br/>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;div className="modal-footer"&gt; <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;div className="button-group d-flex"&gt; <br/>
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;button class="btn btn-link ml-auto"&gt;cancel&lt;/button&gt; <br/>
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;button class="btn btn-info"&gt;sumbit&lt;/button&gt; <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt; <br/>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt; <br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt; <br/>
                              &nbsp;&nbsp;&lt;/div&gt; <br/>
                            &lt;/div&gt; 
                          </code>
                        </p>
                        <div className="button-group">
                          <button class="btn btn-info modal-toggle">Open modal</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-6">
              <div className="row">
                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <div className="card-title">
                        
                      </div>
                      <h2 className="m-10-vc"></h2>
                      <div className="card-content">
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* USE THIS ALL IF U WANT TO USE MODAL*/}
        <div className="modal-wrapper">
          <span className="modal-outsider"></span>
          <div className="modal-wrapper-inner">
            <div className="modal">
              <div className="modal-header">
                Modal Header
              </div>
              <div className="modal-content">
                Modal Content
              </div>
              <div className="modal-footer">
                <div className="button-group d-flex">
                  <button class="btn btn-link ml-auto">cancel</button>
                  <button class="btn btn-info">sumbit</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* USE THIS ALL IF U WANT TO USE MODAL*/}
      </div>
    );
  }
}

$(document).ready(function() {


  
})

export default Modals;