// import React from "react";
import React, { Component } from "react";
import $ from "jquery";

class Wizards extends Component {
  render() {
    return (
      <div className="right-panel">
        <div className="p-20-c">
          <div className="breadcrumb">
            <div className="page-title">Wizard</div>
            <div className="page-info">
              <div className="breadcrumb-item">UI Elements</div>
              <div className="breadcrumb-item">Wizard</div>
            </div>
          </div>
        </div>
        <div className="p-20-c">
          <div className="row" id="index-first-row">
            <div className="col-12">
              <div className="card">
                <div className="card-body">
                  <div className="card-title">
                    option #1
                  </div>
                  <h2 className="m-10-vc">Wizard</h2>
                  <div className="card-content">
                    <p><code>.tabs-group</code> also coming with <code>.wizard</code> that will turn regular <code>.tabs-group</code> into wizard <br/> * note that only one wizard can be use at one page at a time;</p>
                    <p>
                      <code>
                        &lt;div className="tabs-group wizard" id="tg-1"&gt; <br/>
                        &lt;/div&gt;
                      </code>
                    </p>
                    <div className="tabs-group wizard" id="tg-1">
                      <ul className="nav-tabs">
                        <li className="tab-item current" data-tab="tab-1" id="1"><span>1</span> Tab One</li>
                        <li className="tab-item" data-tab="tab-2" id="2"><span>2</span> Tab Two</li>
                        <li className="tab-item" data-tab="tab-3" id="3"><span>3</span> Tab Three</li>
                        <li className="tab-item" data-tab="tab-4" id="4"><span>4</span> Tab Four</li>
                      </ul>
                      <div className="tab-content">
                        <div id="tab-1" className="tab-pane current">
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div>
                        <div id="tab-2" className="tab-pane">
                           Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                        <div id="tab-3" className="tab-pane">
                          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </div>
                        <div id="tab-4" className="tab-pane">
                          Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div>
                      </div>
                    </div>

                    <div className="separator separator-dashed"></div>

                    <p><code>.tabs-group.wizard</code> come with one orientation classes <code>.tabs-left</code></p>
                    <div className="tabs-group tabs-left wizard" id="tg-2">
                      <ul className="nav-tabs">
                        <li className="tab-item current" data-tab="tab-1" id="1"><span>1</span> Tab One</li>
                        <li className="tab-item" data-tab="tab-2" id="2"><span>2</span> Tab Two</li>
                        <li className="tab-item" data-tab="tab-3" id="3"><span>3</span> Tab Three</li>
                        <li className="tab-item" data-tab="tab-4" id="4"><span>4</span> Tab Four</li>
                      </ul>
                      <div className="tab-content">
                        <div id="tab-1" className="tab-pane current">
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div>
                        <div id="tab-2" className="tab-pane">
                           Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                        <div id="tab-3" className="tab-pane">
                          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </div>
                        <div id="tab-4" className="tab-pane">
                          Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

$(document).ready(function() {
  
})

export default Wizards;