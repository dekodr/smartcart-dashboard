// import React from "react";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class Typography extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MemberData: [
        {id: 35, Code: '19450817012', Campaign: 'Buy 1 get 1', Status: 'active', Created: '2-2-2020', Redemption: '17'},
      ]
    }
  }

  render() {

    const items = this.state.MemberData.map((item, key) =>
      <tr id={item.id}>
        <th>{item.Code}</th>
        <td>{item.Campaign}</td>
        <td>
          <button className="btn btn-success btn-xs">{item.Status}</button>
        </td>
        <td>{item.Created}</td>
        <td>{item.Redemption}</td>
        <td>
          <div className="button-row">
            <button className="btn btn-primary btn-icon btn-rad btn-icon-sm"><i class="fas fa-qrcode"></i></button>
            <button className="btn btn-danger btn-icon btn-rad btn-icon-sm"><i className="fas fa-trash"></i></button>
          </div>
        </td>
      </tr>
    );

    return (
      <div className="right-panel">
        <div className="p-20-c">
          <div className="breadcrumb">
            <div className="page-title">Voucher</div>
            <div className="page-info">
              <div className="breadcrumb-item">Voucher</div>
            </div>
          </div>
        </div>
        <div className="p-20-c">
          <div className="row" id="index-first-row">
            <div className="col-12">
              <div className="card">
                <div className="card-body">
                  <div className="card-title">
                    <div className="row">
                      <div className="col-3 d-flex align-item-center">
                        <form action="#" className="form">
                          <div className="input-group">
                            <input type="email" placeholder="email" class="form-control" />
                            <div className="input-group-append">
                              <button className="btn btn-success btn-icon "><i className="fas fa-search"></i></button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div className="col-9 d-flex">
                        <NavLink to="/VoucherAdd" className="ml-auto">
                          <button className="btn btn-success"><i className="fas fa-plus"></i>Add New</button>
                        </NavLink>
                        <button className="btn btn-info"><i class="fas fa-file"></i>Import</button>
                      </div>
                    </div>
                  </div>
                  <div className="card-content">
                    <table className="table">
                      <thead>
                        <tr>
                          <th>Code</th>
                          <th>Campaign</th>
                          <th>Status</th>
                          <th>Created Date</th>
                          <th>Redemption</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {items}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Typography;
