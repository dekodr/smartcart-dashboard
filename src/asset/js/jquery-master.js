import $ from "jquery";

$(document).ready(function() {

  // - - - - - SIDEBAR - - - - -
  $(".menu-list .tablinks:not(:first)").addClass("inactive");
  $(".tabcontent").hide();
  $(".tabcontent:first").show();

  $(".menu-list .tablinks").click(function() {
    var t = $(this).attr("id");
    if ($(this).hasClass("inactive")) {
      //this is the start of our condition
      $(".menu-list .tablinks").addClass("inactive");
      $(this).removeClass("inactive");

      $(".tabcontent").hide();
      $("#" + t + "C").show();
    }
  });

  if ($(".sub-menu").hasClass(".submenu-active")) {
    $(".left-panel").addClass("submenu-active");
  } else {
    $(".left-panel").hover(function() {
      $(".left-panel").addClass("submenu-active");
    });
  }

  // $('.sub-menu .submenu-tab li').click(function() {
  //   $(".left-panel .sub-menu").removeClass("submenu-active");
  //   $(".left-panel .main-menu").removeClass("submenu-active");
  //   $(".left-panel .main-menu .logo-icon").removeClass("submenu-active");
  //   $(".left-panel").removeClass("submenu-active");
  //   $(".navbar-menu").removeClass("submenu-active");
  //   $(".right-panel").removeClass("submenu-active");
  // });

  $(".main-menu .menu-list li").click(function() {
    $(".left-panel .sub-menu").addClass("submenu-active");
    $(".left-panel .main-menu").addClass("submenu-active");
    $(".left-panel .main-menu .logo-icon").addClass("submenu-active");
    $(".left-panel").addClass("submenu-active");
    $(".navbar-menu").addClass("submenu-active");
    $(".right-panel").addClass("submenu-active");
  });

  $(".submenu-tab ul li").click(function() {
    $(".right-panel").addClass("submenu-active");
  })

  $(".right-panel").click(function() {
    $(".left-panel .sub-menu").removeClass("submenu-active");
    $(".left-panel .main-menu").removeClass("submenu-active");
    $(".left-panel .main-menu .logo-icon").removeClass("submenu-active");
    $(".left-panel").removeClass("submenu-active");
    $(".navbar-menu").removeClass("submenu-active");
    $(this).removeClass("submenu-active");
  });
  // SIDEBAR - - - END

  $(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 30) {
      $(".navbar-menu").addClass("scroll-active");
    } else {
      $(".navbar-menu").removeClass("scroll-active");
    }
  });

  // - - - - - - - - - D R O P I F Y - - - - - - - - -
  $('.dropify').dropify();
  // - - - - - - - D R O P I F Y  E N D - - - - - - -

  // - - - - - - - - - F O R M  G R O U P - - - - - - - - -
  $('.form-group.disabled').children('input').attr('disabled','true');

  $('.form-group').mouseover(function() {
    $(this).addClass('focused');
    $(this).children('input').focus();
  }).mouseout(function() {
    $(this).removeClass('focused');
    $(this).children('input').focus();
  });
  // - - - - - - - F O R M  G R O U P  E N D - - - - - - -

  // - - - - - - - - - R A D I O  J S - - - - - - - - -
  $('.radio-label').click(function(e) {
    e.stopPropagation();

    if (!$(this).hasClass('checked')) {
      if ($(this).is('#radio-label1')) {
        $( "#radio1" ).prop( "checked", true );
        $( "#radio2" ).prop( "checked", false );
        $('#radio-label1').addClass('checked');
        $('#radio-label2').removeClass('checked');
      } else {
        $( "#radio1" ).prop( "checked", false );
        $( "#radio2" ).prop( "checked", true );
        $('#radio-label1').removeClass('checked');
        $('#radio-label2').addClass('checked');
      }
    } else {
      $( "#radio1" ).prop( "checked", false );
      $( "#radio2" ).prop( "checked", false );
      $(this).removeClass('checked');
    }

  });
  // - - - - - - - R A D I O  J S  E N D - - - - - - -


  // - - - - - - - - - S W I T C H  J S - - - - - - - - -
  $('.switch-label').click(function(e) {
    e.stopPropagation();

    $(this).siblings('.switch').click();

  });
  // - - - - - - - S W I T C H  J S  E N D - - - - - - - 


  // - - - - - - - - - F I L E  I N P U T  J S - - - - - - - - -
  $('#chooseFile').bind('change', function () {
    var filename = $("#chooseFile").val();
    if (/^\s*$/.test(filename)) {
      $(".file-upload").removeClass('active');
      $("#noFile").text("No file chosen..."); 
    }
    else {
      $(".file-upload").addClass('active');
      $("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
    }
  });
  // - - - - - - - F I L E  I N P U T  J S  E N D - - - - - - -


  // - - - - - - - - - S O R T A B L E  T A B L E - - - - - - - - -
  $('table.table').tablesorter({
      // customize header HTML
      onRenderHeader: function(index) {
          // the span wrapper is added by default
          this.wrapInner('<span class="icons"></span>');
      },
      cssAsc: 'up',
      cssDesc: 'down',
  });
  // - - - - - - - S O R T A B L E  T A B L E   E N D - - - - - - -


  // - - - - - - - - - D A T A  T A B L E - - - - - - - - -
  $(function() {
    var rowsPerPage = parseInt($('.dataTable-EntriesOpt select').children("option:selected").val());
    let table_id = $('.table.add-pagination').attr('id');
    let rows = $('.table.add-pagination#'+table_id+' tbody tr');
    const rowsCount = rows.length;
    const pageCount = Math.ceil(rowsCount / rowsPerPage); // avoid decimals
    const wrapper = $('.pagination #wrapper');
    let numbers = '';
    let middleWidthLimit = 0;
    let i = 1;
    var start;
    var end;

    // -=-=-=-=-=- PAGINATION -=-=-=-=-=-=-=-=-=-=-
    if (pageCount > 5) {
      middleWidthLimit = 5;
    } else {
      middleWidthLimit = pageCount;
    }
    // Generate the pagination.
    for (i ; i <= pageCount; i++) {
      numbers += '<li><a href="#" id="' + (i) + '">' + (i) + '</a></li>';
    }
    wrapper.append('<li className="Prev" id="prev-page"><span>Previous</span></li><li class="middle" style="width:'+37*parseInt(middleWidthLimit)+'px"><ol id="numbers">'+numbers+'</ol><li className="Next" id="next-page"><span>Next</span></li>')
      
    // Mark the first page link as active.
    $('#numbers li:first-child').addClass('active');
    // Display the first set of rows.
    displayRows(1);

    // next btn
    $('#next-page').click(function(){
      const clicked_id = $('#numbers li.active a').attr('id');
      if (clicked_id > 2 && clicked_id < (parseInt(pageCount)-2)) {
        $('#wrapper li.middle ol').css( 'left', -(37*(parseInt(clicked_id)-2)) )
        console.log("width = 37 ; clicked_id = "+parseInt(clicked_id)+" ; -37 * "+(parseInt(clicked_id)-2)+' = '+-(37*(parseInt(clicked_id)-2)))
      }
      // move number through next-btn
      if (clicked_id != pageCount) {
        displayRows(parseInt(clicked_id) + 1);
        $('#numbers li.active a#'+clicked_id).parent().removeClass('active').next().addClass('active');
      }
    });

    // prev btn
    $('#prev-page').click(function(){
      const clicked_id = $('#numbers li.active a').attr('id');
      if (clicked_id > 2 && clicked_id < (parseInt(pageCount)-2)) {
        $('#wrapper li.middle ol').css( 'left', -(37*(parseInt(clicked_id)-3)) )
        console.log('width = -37 ; clicked_id = '+parseInt(clicked_id)+' ; 37 * '+(parseInt(clicked_id)-3)+' = '+-(37*(parseInt(clicked_id)-3)))
      }
      // move number through prev-btn
      if (clicked_id != 1) {
        displayRows(parseInt(clicked_id) - 1);
        $('#numbers li.active a#'+clicked_id).parent().removeClass('active').prev().addClass('active');
      }
    });
    
    // On pagination click.
    $('#numbers li a').click(function(e) {
      var $this = $(this);
      const clicked_id = $(this).attr('id');
      
      e.preventDefault();
      
      // scroll through click of number [CHECKED]
      if (clicked_id < 5) {
        $('#wrapper li.middle ol').css('left',0 );
      } else if (clicked_id > (parseInt(pageCount)-2)) {
        $('#wrapper li.middle ol').css('left', -(37*(parseInt(pageCount)-5)))
      } else {
        $('#wrapper li.middle ol').css('left',-(37*(parseInt(clicked_id)-3)) )
      }
      // Remove the active class from the links.
      $('#numbers li').removeClass('active');
      
      // Add the active class to the current link.
      $this.parent().addClass('active');
      
      // Show the rows corresponding to the clicked page ID.
      displayRows($this.text());
    });
    
    // Function that displays rows for a specific page.
    function displayRows(index) {
      const dataTableInfo = $('.dataTable-info h4');
      start = (index - 1) * rowsPerPage;
      end = start + rowsPerPage;
      
      // Hide all rows.
      rows.hide();
      
      // Show the proper rows for this page.
      rows.slice(start, end).show();

      // -=-=-=-=-=- DATA TABLE INFO -=-=-=-=-=-=-=-=-=-=-
      dataTableInfo.text('Showing '+(parseInt(start)+1)+' to '+parseInt(end)+' of '+rowsCount+' entries')
      // -=-=-=-=-=- DATA TABLE INFO - - - END -=-=-=-=-=- 
    }

    // -=-=-=-=-=- DATA TABLE SEARCH -=-=-=-=-=-=-=-=-=-=-
    $(".dataTable-wrapper #search").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      const dataTableInfo = $('.dataTable-info h4');

      if (value != '') {
        rows.filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      } else {
        displayRows($('#numbers li.active a').attr('id'));
      }
    });
    // -=-=-=-=-=- DATA TABLE SEARCH - - - END -=-=-=-=-=-
    // -=-=-=-=-=- PAGINATION - - - END -=-=-=-=-=- 
  });
  
  // - - - - - - - D A T A  T A B L E   E N D - - - - - - -


  // - - - - - - - W I Z A R D - - - - - - -
  const wizard = $('.tabs-group.wizard');
  wizard.append('<div class="tab-action d-flex"><button class="btn btn-success ml-auto prev-wizard">Previous</button><button class="btn btn-success next-wizard">Next</button></div>');

  $('.next-wizard').click(function() {
    let wizard_id = $(this).parent().parent().attr('id');
    const currentPage = $('.tabs-group.wizard#'+wizard_id+' ul.nav-tabs li.tab-item.current').attr('id');
    // alert(currentPage);
    let nextPage = parseInt(currentPage) + 1;

    $('.tabs-group.wizard#'+wizard_id+' ul.nav-tabs li.tab-item#'+nextPage).trigger('click');
    alert(currentPage+"  "+wizard_id+"  "+nextPage);
  });

  $('.prev-wizard').click(function() {
    let wizard_id = $(this).parent().parent().attr('id');
    const currentPage = $('.tabs-group.wizard#'+wizard_id+' ul.nav-tabs li.tab-item.current').attr('id');
    // alert(currentPage);
    let nextPage = parseInt(currentPage) - 1;

    $('.tabs-group.wizard#'+wizard_id+' ul.nav-tabs li.tab-item#'+nextPage).trigger('click');
    // alert(currentPage);
  });
  // - - - - - - - W I Z A R D   E N D - - - - - - -


  // - - - - - - - M O D A L  T O G G L E - - - - - - -
  $('.modal-toggle').click(function() {
    $('.modal-wrapper').addClass('modal-active');
    $('.modal').addClass('modal-active');
  })

  $('.modal-wrapper span.modal-outsider').click(function() {
    $('.modal-wrapper').removeClass('modal-active');
    $('.modal').removeClass('modal-active');
  })
  // - - - - - - - M O D A L  T O G G L E   E N D - - - - - - -

});





