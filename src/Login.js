import React, { Component } from "react";
import $ from "jquery";
import { Route, HashRouter } from "react-router-dom";
import { NavLink } from "react-router-dom";

import "./asset/scss/index.scss";
import "./asset/js/jquery-master.js";
import logo from "./asset/images/logo-1.png"


class Login extends Component {
  render() {
    return (
      <HashRouter>
        <div className="master-body login">
          
          <div className="background-frame" />
          <div className="master-frame">
            <div className="master-inner">
              <img src={logo} className="login-logo" />
              <div className="form">
                <div className="row username-wp">
                  <div className="col-12">
                    <div className="form-group form-group-default">
                      <label htmlFor="#" className="form-label">username</label>
                      <input type="text" className="form-control" />
                    </div>
                  </div>
                </div>
                <div className="row password-wp">
                  <div className="col-12">
                    <div className="form-group form-group-default">
                      <label htmlFor="#" className="form-label">password</label>
                      <input type="password" className="form-control" />
                    </div>
                  </div>
                </div>
                <div className="row rememberMe-wp">
                  <div className="col-6">
                    <div className="checkbox-control">
                      <input type="checkbox" id="checkbox-1" />
                      <label htmlFor="checkbox-1">remember me</label>
                    </div>
                  </div>
                </div>
                <div className="row button-wp">
                  <div className="col-12">
                    <NavLink exact to="/App">
                      <button class="btn btn-success btn-fullwidth btn-login">Login</button>
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default Login;
