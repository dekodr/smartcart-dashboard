import React, { useState } from 'react';
import $ from "jquery";

let component;
let addOn;

function handleInput(phrase) {
  alert(phrase);
}

function Input(props) {

  const [phrase, setPhrase] = useState('');
  // check if it's got the data entered
  // alert(phrase);

  // check input render type
  if (props.type == 'modern') {
    component =
    <div className="form-group">
      <label htmlFor="#" className="form-label">username</label>
      <input 
        type="text" 
        className="form-control" 
        value={phrase}
        onChange={e => setPhrase(e.target.value)}/>
    </div>
  } else {
    component =
    <div className="form-group form-group-default">
      <label htmlFor="#" className="form-label">username</label>
      <input type="text" className="form-control" />
    </div>
  }
  return (
    <div className="form">
      {component}
    </div>
  );
}

$(window).scroll(function() {
  
});

export default Input;
