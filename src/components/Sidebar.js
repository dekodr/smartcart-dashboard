import React from "react";
import { NavLink } from "react-router-dom";
import $ from "jquery";

function SideBar() {
  return (
    <div className="left-panel" id="menu-tabs">
      <div className="main-menu">
        <div className="logo-icon" />
        <ul className="menu-list">
          <li className="tablinks" id="tab1">
            <i className="fas fa-home" />
            <div className="caption">Dashboards</div>
          </li>
          <li className="tablinks" id="tab2">
            <i className="far fa-calendar-alt" />
            <div className="caption">Daily</div>
          </li>
          <li className="tablinks" id="tab3">
            <i className="fas fa-users" />
            <div className="caption">Member</div>
          </li>
          <li className="tablinks" id="tab4">
            <i className="fas fa-barcode" />
            <div className="caption">Products</div>
          </li>
          <li className="tablinks" id="tab5">
            <i className="fas fa-list-ul" />
            <div className="caption">Categories</div>
          </li>
          <li className="tablinks" id="tab6">
            <i className="fas fa-sitemap" />
            <div className="caption">UI</div>
          </li>
        </ul>
      </div>

      <div className="sub-menu">
        <div className="inner">
          <div className="submenu-tab tabcontent" id="tab1C">
            <ul>
              <li>
                <NavLink exact to="App/">
                  <i className="fas fa-chart-pie" /> Default
                </NavLink>
              </li>
              <li>
                <a href="test">
                  <i className="fas fa-chart-pie" /> Analytics
                </a>
              </li>
              <li>
                <a href="test">
                  <i className="fas fa-chart-pie" /> Ecommerce
                </a>
              </li>
              <li>
                <a href="test">
                  <i className="fas fa-chart-pie" /> Content
                </a>
              </li>
              <li>
                <a href="test">
                  <i className="fas fa-chart-pie" /> Application
                </a>
              </li>
              <li>
                <NavLink to="/Login">
                  <i className="fas fa-chart-pie" /> Login
                </NavLink>
              </li>
            </ul>
          </div>
          <div className="submenu-tab tabcontent" id="tab2C">
            <ul>
              <li>
                <a href="test">
                  <i className="fas fa-chart-pie" /> Menu tab #2
                </a>
              </li>
              <li>
                <a href="test">
                  <i className="fas fa-chart-pie" /> Menu tab #2
                </a>
              </li>
              <li className="active">
                <a href="test">
                  <i className="fas fa-chart-pie" /> Menu tab #2
                </a>
              </li>
              <li>
                <a href="test">
                  <i className="fas fa-chart-pie" /> Menu tab #2
                </a>
              </li>
            </ul>
          </div>
          <div className="submenu-tab tabcontent" id="tab3C">
            <ul>
              <li>
                <NavLink to="/Member">
                  <i className="fas fa-chart-pie" /> Member
                </NavLink>
              </li>
            </ul>
          </div>
          <div className="submenu-tab tabcontent" id="tab4C">
            <ul>
              <li className="active">
                <NavLink to="/Voucher">
                  <i className="fas fa-chart-pie" /> Voucher
                </NavLink>
              </li>
            </ul>
          </div>
          <div className="submenu-tab tabcontent" id="tab5C">
            <ul>
              <li className="active">
                <a href="test">
                  <i className="fas fa-chart-pie" /> Menu tab #5
                </a>
              </li>
            </ul>
          </div>
          <div className="submenu-tab tabcontent" id="tab6C">
            <ul>
              <li>
                <NavLink to="/Button">
                  <i className="fas fa-font" /> Buttons
                </NavLink>
              </li>
              <li>
                <NavLink to="/DataTable">
                  <i className="fas fa-font" /> Data Table
                </NavLink>
              </li>
              <li>
                <NavLink to="/Form">
                  <i className="fas fa-font" /> Forms
                </NavLink>
              </li>
              <li>
                <NavLink to="/Modal">
                  <i className="fas fa-font" /> Modals
                </NavLink>
              </li>
              <li>
                <NavLink to="/Tabs">
                  <i className="fas fa-font" /> Tabs & accordions
                </NavLink>
              </li>
              <li>
                <NavLink to="/Table">
                  <i className="fas fa-font" /> Table
                </NavLink>
              </li>
              <li>
                <NavLink to="/Wizard">
                  <i className="fas fa-font" /> Wizard
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

$(document).ready(function() {
  
});

export default SideBar;
